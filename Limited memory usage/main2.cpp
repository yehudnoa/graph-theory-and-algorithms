#ifndef __PROGTEST__

#include <ctime>
#include "flib.h"
#endif //__PROGTEST__

template <class T> void swap ( T& a, T& b )
{
    T c(a); a=b; b=c;
}

void buildMaxHeap(int * arr, int n)
{
    for (int i = 1; i < n; i++)
    {
        if (arr[i] > arr[(i - 1) / 2])
        {
            int j = i;
            while (arr[j] > arr[(j - 1) / 2])
            {
                swap(arr[j], arr[(j - 1) / 2]);
                j = (j - 1) / 2;
            }
        }
    }
}



void heapSort(int * arr, int n)
{
    buildMaxHeap(arr, n);

    for (int i = n - 1; i > 0; i--)
    {
        swap(arr[0], arr[i]);
        int j = 0, index;

        index = (2 * j + 1);
        do
        {
            if (arr[index] < arr[index + 1] &&
                index < (i - 1))
                index++;

            if (arr[j] < arr[index] && index < i)
                swap(arr[j], arr[index]);

            j = index;
            index = (2 * j + 1);

        } while (index < i);
    }
}

void mergeFiles(int32_t file1, int32_t file2, int32_t out_file, int32_t bytes, int * q){
    int loaded, loaded1, place, a, b, c;
    bool l = false, l1 = false;

    flib_open(file1, READ);
    flib_open(file2, READ);
    flib_open(out_file, WRITE);
    loaded = flib_read(file1, q, bytes/2);
    a = q[loaded - 1];
    b = 2;

    loaded1 = flib_read(file2, &q[loaded], bytes - loaded);
    place = loaded + loaded1;

    if(a > q[place - 1]){
        a = q[place - 1];
        b = 1;
    }


    while(!l || !l1){
        c = 0;
        heapSort(q,place);
        for(loaded = place - 1; q[loaded] != a; loaded--) {
            ;
        }
        flib_write(out_file, q, loaded + 1);
        loaded = place - loaded - 1;

        for(int i = 0; i < loaded; i++){
            q[i] = q[ place - loaded + i];
        }

        place = loaded;
        if((place + bytes/2) == bytes || l || l1 ){
            a = q[place - 1];
            c = 1;
        }


        if(b == 1){
            if(!l1 && !l){
                if(place + (bytes/2) < bytes){
                    loaded1 = flib_read(file2, &q[place], bytes/2);
                } else {
                    loaded1 = flib_read(file2, &q[place], bytes - place);
                }
                if(loaded1 == 0){
                    l1 = true;
                }
                place = place + loaded1;

                if (a > q[place - 1] || c != 1) {
                    a = q[place - 1];
                    b = 1;
                }

                loaded = flib_read(file1, &q[place], bytes - place);
                place = place + loaded;
                if(loaded == 0 && place != bytes){
                    l = true;
                }
                if (a > q[place - 1] ) {
                    a = q[place - 1];
                    b = 2;
                }
            } else if (!l){

                loaded = flib_read(file1, &q[place], bytes - place);
                place = place + loaded;
                if(loaded == 0 && place != bytes){
                    l = true;
                }
                if (a > q[place - 1] ) {
                    a = q[place - 1];
                    b = 2;
                }
            }else {
                loaded1 = flib_read(file2, &q[place], bytes - place);
                if(loaded1 == 0){
                    l1 = true;
                }
                place = place + loaded1;
                if (a > q[place - 1]) {
                    a = q[place - 1];
                    b = 1;
                }
            }
        } else {
            if(!l1 && !l){
                if(place + (bytes/2) < bytes){
                    loaded1 = flib_read(file1, &q[place], bytes/2);
                } else {
                    loaded1 = flib_read(file1, &q[place], bytes - place);
                }
                place = place + loaded1;
                if(loaded1 == 0){
                    l = true;
                }
                if (c != 1 || a > q[place - 1]) {
                    a = q[place - 1];
                    b = 2;
                }
                loaded = flib_read(file2, &q[place], bytes - place);
                if(loaded == 0 && place != bytes){
                    l1 = true;
                }
                place = place + loaded;
                if (a > q[place - 1]) {
                    a = q[place - 1];
                    b = 1;
                }
            } else if (!l1){
                loaded = flib_read(file2, &q[place], bytes - place);
                if(loaded == 0 && place != bytes){
                    l1 = true;
                }
                place = place + loaded;
                if (a > q[place - 1]) {
                    a = q[place - 1];
                    b = 1;
                }
            }else{
                loaded1 = flib_read(file1, &q[place], bytes - place);
                if(loaded1 == 0){
                    l = true;
                }
                place = place + loaded1;
                if (a > q[place - 1]) {
                    a = q[place - 1];
                    b = 2;
                }
            }
        }
    }


    if(place != 0){
        heapSort(q, place);
        flib_write(out_file, q, place);
    }
    flib_close(file1);
    flib_close(file2);
    flib_remove(file1);
    flib_remove(file2);
    flib_close(out_file);
}


void tarant_allegra ( int32_t in_file, int32_t out_file, int32_t bytes ){
    flib_open(in_file, READ);
    int loaded, idFiles = 1, newfile;
    bytes /= sizeof(int32_t);
    bytes -= 10;

    /// Use sorting by binary heap and insert to files
    int *q = new int [bytes];
    while(true) {
        loaded = flib_read(in_file, q, bytes);
        if(loaded == 0) {

            break;
        }
        idFiles++;
        heapSort(q, loaded);
        flib_open(idFiles , WRITE);
        flib_write(idFiles , q , loaded);
        flib_close(idFiles);
        printf("\n\n");
    }
    flib_close(in_file);

    newfile = 2;
    ///go through files till only 2 files left

    ////////////////////////////////////////////////////////////////////////////////////////////
    if(idFiles==2){
        flib_open(idFiles, READ);
        flib_open(out_file , WRITE);
        loaded = flib_read(idFiles,q,bytes);
        flib_write(out_file , q , loaded);
        flib_close(idFiles);
        flib_close(out_file);
    } else{
        while(newfile+1 != idFiles){
            mergeFiles(newfile , newfile+1,idFiles+1,bytes, q);
            newfile+=2;
            idFiles++;
        }
        mergeFiles(newfile,newfile+1,out_file,bytes, q);
    }
    delete [] q;
}

#ifndef __PROGTEST__

uint64_t total_sum_mod;
void create_random(int output, int size){
    total_sum_mod=0;
    flib_open(output, WRITE);
    srand(time(NULL));
#define STEP 100ll
    int val[STEP];
    for(int i=0; i<size; i+=STEP){
        for(int j=0; j<STEP && i+j < size; ++j){
            val[j]=-100000+ (rand()%(100*10000+1));
            total_sum_mod += val[j];
        }
        flib_write(output, val, (STEP < size-i) ? STEP : size-i);
    }
    flib_close(output);
}

void tarant_allegra ( int in_file, int out_file, int bytes );

void check_result ( int out_file, int SIZE ){
    flib_open(out_file, READ);
    int q[30], loaded, last=-(1<<30), total=0;
    uint64_t current_sum_mod=0;
    while(loaded = flib_read(out_file, q, 30), loaded != 0){
        total += loaded;
        for(int i=0; i<loaded; ++i){
            if(last > q[i]){
                printf("the result file contains numbers %d and %d on position %d in the wrong order!\n", last, q[i], i-1);
                exit(1);
            }
            last=q[i];
            current_sum_mod += q[i];
        }
    }
    if(total != SIZE){
        printf("the output contains %d but the input had %d numbers\n", total, SIZE); exit(1);
    }
    if(current_sum_mod != total_sum_mod){
        printf("the output numbers are not the same as the input numbers\n");
        exit(1);
    }
    flib_close(out_file);
}

int main(int argc, char **argv){
    const uint16_t MAX_FILES = 65535;
    flib_init_files(MAX_FILES);
    int INPUT = 0;
    int RESULT = 1;
    int SIZE = 3500000;

        create_random(INPUT, SIZE);
        tarant_allegra(INPUT, RESULT, 2000);
        check_result(RESULT, SIZE);

//    create_random(INPUT, SIZE);
//    tarant_allegra(INPUT, RESULT, 2000);
//    check_result(RESULT, SIZE);

    flib_free_files();
    return 0;
}
#endif //__PROGTEST__