#include <iostream>
#include <vector>

struct planet {
    int pNum;
    std::vector<int> connections;
    int prev = -1,  goTo ;


    bool infection = false, closed = false, visited = false;

    planet(int num){
        this->pNum = num;
    }

    void addConnection(int tmp){
        connections.emplace_back(tmp);
    }
};

int main() {

    int vertices, edges, start, end, amount, tmp, tmp1;
    std::vector<planet> V;

    std::cin >> vertices >> edges ;
    std::cin >> start >> end >> amount;

    V.reserve(vertices);
    for (int i = 0 ; i < vertices; ++i){
        V.emplace_back(i);
    }
    std::cin >> tmp;
    for (int i = 0; i < tmp; ++i){
        std::cin >> tmp1;
        V[tmp1].infection = true;
    }
    std::cin >> tmp;
    for (int i = 0; i < tmp; ++i){
        std::cin >> tmp1;
    }
    for (int i = 0; i < edges; ++i){
        std::cin >> tmp >> tmp1;
        V[tmp].addConnection(V[tmp1].pNum);
        V[tmp1].addConnection(V[tmp].pNum);
    }
    tmp = -5;
    bool reached = false;
    int place = start;
    do{
        V[place].visited = true;

        for(int i = 0; i < (int)V[place].connections.size(); ){
            V[place].visited = true;
            if ( tmp == 0 ){ V[place].visited = false; break; }
            if(!V[V[place].connections[i]].visited && V[V[place].connections[i]].prev != V[place].pNum){
                V[V[place].connections[i]].prev = V[place].pNum;
                V[place].goTo = V[place].connections[i];
                place = V[V[place].connections[i]].pNum;
                if( V[place].infection && tmp == -5){ tmp = amount; }
                else if ( tmp != -5 ){ tmp -= 1; }
                i = 0;
            } else {
                i++;
            }
            if(V[place].pNum == end){
                reached = true;
                break;
            }
        }
        V[place].closed = true; place = V[V[place].prev].pNum;
        if (tmp != -5 && tmp != amount){ tmp += 1; }
        if (tmp == amount){ tmp = -5; }
    }
    while (!V[start].closed && !reached);

    if(reached){
        for (int i = start; i != end; ){
            std::cout << V[i].pNum << " ";
            i = V[i].goTo;
        }
        std::cout << end;
    } else {
        std::cout << "-1" << std::endl;
    }
    return 0;
}
