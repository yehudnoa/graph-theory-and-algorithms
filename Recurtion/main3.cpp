#include <iostream>
#include <vector>
#include <cmath>
class protocal{
public:
    int start, active, stop;
    double dR;
    bool activate = true, on = false;
    protocal(int a, int b, int c, double d){
        start = a;
        active = b;
        stop = c;
        dR = d;
    }
};

class blabla{
public:
    int week, guidline;
    bool on;

    blabla(int a, int b, bool c){
        week = a; guidline = b; on = c;
    }
};

class week{
public:
    int  infected, popularity, minPop = -1;
    bool election = false;
    std::vector<blabla> keep;
};
int ourMin = 1073741825;
std::vector<blabla> finalResult;

void solve(int weekNum, int max, double R, std::vector<protocal> guidlines, std::vector<week> weeks, std::vector<int> prot){

    int tmp;
    double DR = 1;
    std::vector<int> tmpProt;
    weeks[weekNum].popularity = fmin(100, weeks[weekNum].popularity);
    if( weeks[weekNum].popularity < 0 ){
        return;
    }else if( weeks[weekNum].infected > 1073741824 ){
        return;
    } else if( weeks[weekNum].election && weeks[weekNum].popularity < weeks[weekNum].minPop ){
        return;
    } else if( weekNum == (int)weeks.size() - 1) {
        if(weeks[weekNum].infected < ourMin){
            ourMin = weeks[weekNum].infected;
            finalResult = weeks[weekNum].keep;
        }
        return;
    }

    weeks[weekNum + 1].popularity = weeks[weekNum].popularity;
    weeks[weekNum + 1].keep = weeks[weekNum].keep;
    for(auto i : prot){
        DR *= guidlines[i].dR;
        weeks[weekNum + 1].popularity += guidlines[i].active;
    }

    ///if max amount of guidlies are already set
    if( (int)prot.size() == max ){
        for(int i = 0; i < (int)prot.size(); i++){
            tmp = floor(0.00000001 + weeks[weekNum].infected * (R * (DR / guidlines[prot[i]].dR )));
            weeks[weekNum + 1].infected = tmp;
            tmpProt = prot;
            if(prot[i] != 0){
                tmpProt.erase(tmpProt.begin() + i);
                guidlines[prot[i]].activate = false;
                weeks[weekNum + 1].popularity += guidlines[prot[i]].stop - guidlines[prot[i]].active;
                weeks[weekNum + 1].keep.emplace_back(weekNum + 1, prot[i], false);
                solve(weekNum + 1, max, R, guidlines, weeks, tmpProt);
                weeks[weekNum + 1].keep.erase(weeks[weekNum + 1].keep.end() - 1);
                weeks[weekNum + 1].popularity -= guidlines[prot[i]].stop - guidlines[prot[i]].active;
                guidlines[prot[i]].activate = true;
            } else {
                solve(weekNum + 1, max, R, guidlines, weeks, tmpProt);
            }
        }
        ///try all possible paths
    } else {
        for( int i = 0; i < (int)guidlines.size(); i++){
            ///first guidline is always no guidline
            if(i == 0){
                tmp = floor(0.00000001 + weeks[weekNum].infected * (R * DR ));
                weeks[weekNum + 1].infected = tmp;
                solve(weekNum + 1, max, R, guidlines, weeks, prot);
                ///if guidline still active
            }else if(guidlines[i].activate){
                ///if guidline on then try path were cancled
                if( guidlines[i].on ){
                    tmp = floor(0.00000001 + weeks[weekNum].infected * (R * (DR / guidlines[i].dR )));
                    weeks[weekNum + 1].infected = tmp;
                    weeks[weekNum + 1].popularity += guidlines[i].stop - guidlines[i].active;
                    tmpProt = prot;
                    for(auto it = tmpProt.begin(); it != tmpProt.end(); it++){
                        if(*it == i){
                            tmpProt.erase(it);
                            break;
                        }
                    }
                    guidlines[i].activate = false;
                    weeks[weekNum + 1].keep.emplace_back(weekNum + 1, i, false);
                    solve(weekNum + 1, max, R, guidlines, weeks, tmpProt);
                    weeks[weekNum + 1].popularity -= guidlines[i].stop - guidlines[i].active;
                    weeks[weekNum + 1].keep.erase(weeks[weekNum + 1].keep.end() - 1);
                    guidlines[i].activate = true;
                    ///else try path were guidline is added
                } else {
                    tmp = floor(0.00000001 + weeks[weekNum].infected * (R * (DR * guidlines[i].dR )));
                    weeks[weekNum + 1].infected = tmp;
                    weeks[weekNum + 1].popularity += guidlines[i].start;
                    tmpProt = prot;
                    tmpProt.emplace_back(i);
                    guidlines[i].on = true;
                    weeks[weekNum + 1].keep.emplace_back(weekNum + 1, i, true);
                    solve(weekNum + 1, max, R, guidlines, weeks, tmpProt);
                    weeks[weekNum + 1].popularity -= guidlines[i].start;
                    weeks[weekNum + 1].keep.erase(weeks[weekNum + 1].keep.end() - 1);
                    guidlines[i].on = false;
                }
            }
        }
    }
}
int main() {
    int T, N, A, O, P, F, V, tmp, tmp1, tmp2;
    std::vector <protocal> guidlines;
    std::vector <week> weeks;
    std::vector <int> prot;
    double R, tmp3;
    std::cin >> T;
    for(int j = 0; j < T; j++){
        ourMin = 1073741825;
        finalResult.clear();
        guidlines.clear();
        weeks.clear();
        prot.clear();
        prot.emplace_back(0);
        std::cin >> N >> A >> O >> P >> F >> V >> R;
        weeks.resize(N);
        guidlines.emplace_back(0, 0, 0, 1.0);
        for(int i = 0; i < O; i++){
            scanf(" %d %d %d %lf", &tmp, &tmp1, &tmp2, &tmp3);
            guidlines.emplace_back(tmp, tmp1, tmp2, tmp3);
        }
        for(int i = 0; i < P; i++){
            std::cin >> tmp >> tmp1;
            weeks[tmp - 1].election = true;
            weeks[tmp - 1].minPop = tmp1;
        }
        weeks[0].infected = V;
        weeks[0].popularity = F;
        solve(0, A + 1, R, guidlines, weeks, prot);

        if(ourMin == 1073741825){
            std::cout << "-1" << std::endl;
        } else {
            std::cout<< (int) finalResult.size() << " " <<ourMin<<std::endl;
            for(int i = 0; i < (int)finalResult.size(); i++){
                std::cout<< finalResult[i].week + 1 << " ";
                if(finalResult[i].on){
                    std::cout<< "zavest";
                } else {
                    std::cout<< "zrusit";
                }
                std::cout<< " " << finalResult[i].guidline - 1 << std::endl;
            }
        }
    }

    return 0;
}