# Graph Theory and Algorithms

Homework's from Algorithms' and graphs 1 course

**DFS Task Discription-**
It’s year 3000 and the whole universe is infected with an unknown virus. The virus is so dangerous that the pandemic 1000 years ago, which spread across the whole civilized world, seems like a common cold in comparison. This virus destroys not just any living thing but also whole planets, as seen in the picture we got from the Hubble telescope.

Even though the best brains of this generation all gathered on the university on planet Ankh-Morpork the vaccine development is basically at point 0. Scientists need a sample of this virus to have a good chance of developing a vaccine. The good news is that the crew of Planet Express spaceship got an RNA sample.

The battle is not won yet! The virus is spreading and we need the sample delivered from the current ship location to the university. This is why they decided to travel light and fast. Still, they need to refuel on every planet they visit.

Now we need to create a path-plan for the ship. The crew has a map of the known universe where all the infected planets are marked. Each planet is also shown to be connected to few other planets they are able to travel between in on one full tank. They can travel to infected planets as well but they get infected which is acceptable to them. The infected crew can survive only few jumps but if they manage to get to the university in time scientists believe they can save crew’s lives.

Not only the human race is creating a vaccine. There are aliens which claim to have it. As their planets were not infected yet their vaccine may be working. The crew can pay these worlds to visit and cure their infection.

Your task is to decide whether there is a path from the current ships location to the university. If the crew survives and makes it to the planet you should print out the planets in order their visited them. If there is no possible way to make it to the university alive, then … you should treasure the remaining time with your loved ones.

**Input format**
On the first line, there are two integers N and M specifying the number of planets in the known universe and number of known space paths, respectively. The numbering of planets is zero-base, their identifiers are thus 0,…,N-1.
On the second line, there are three numbers s, t, and l. The first number stands for the initial planet, the second number stands for the terminal planet and the third is a number of spaceship stops that a crew lasts when infected.
On the third line, there is a number K specifying the number of actually infected planets. If K > 0, then this line is followed by K identificators of actually infected planets.
On the next line, there is a number D specifying the number of planets where an experimental vaccine can be bought. Of D > 0, then this line is followed by D identificators of such planets.
After that follows M lines each consisting out of two integers x y, 0 ≤ x,y < N, which identifies two planets that are mutually achievable on a single tank.
Input is always valid.
The initial and the terminal planet are neither infected, nor a vaccine can be bought on them.
According to problem definition, there is no planet v such that v is infected and an experimental vaccine can be bought there at the same time.
Output format
The output consists of a single line. If there is any possible walk between planet s and planet t, you have to output this walk. Otherwise, output number -1.
A walk between s and t is usually not exclusive. You can output arbitrary one.
Walks that are longer than max(3D*N, N) planets will be refused.




***Limited Memory Usage Task Discription***
To finish your current task, you are required to sort a huge amount of numbers. As luck would have it, the University supercomputer is fully occupied by the analysis of the RNA sample which was delivered thanks to your efficient pathfinding algorithm from the previous task.

Therefore you decided to return to simpler times, when 640 kB of memory was enough for everyone, and to run your sorting algorithm on your personal computer. Luckily, you already finished a library which simplifies file handling and you even have its documentation!

**Implementation notes**
The program starts by calling your implementation of the function tarant_allegra(), which receives three integer parameters. The first is H, which is the ID of the input file. The input file contains the numbers which are to be sorted. The second parameter is P, the ID of the output file. After running your algorithm the output file should contain the same numbers as the input file, but in ascending order. The third parameter gives you number of bytes you are allowed to use on the program's heap. You may only to read sequentially from H and only write sequentially into P. The input file cannot be deleted.

While you program is running you may create your own auxiliary files. This is enabled by the supplied API. The total number of files is limited by the datatype which represents the files' ID. You can use any method in the supplied API to modify these files. You can assume that no errors occurs while handling the files, except those implied from the API restructions. The total size of the auxiliary files must never exceed twice the size of the input file.

You program is guaranteed some working memory (stack + heap) given in the input description below. If you use more memory, your program will be killed and your solution will be regarded as invalid.

STL it not allowed as the computer is not efficient enought to run it.

**API**
API flib.h, its description and scaffolding of the empty solution can be downloaded from Sample data on the bottom of this task's description. API throws exceptions when it is used incorrectly. The API works as follows:

Every file may be opened at most once at each moment. Each file may be opened either for reading or for writing.
When the file is opened the read/write pointer points to its beginning.
Reading and writing are done sequentially, i.e., data is read and written one by one in a sequence. It is impossible to jump around the file.
File can be removed if it is not currently open. Input file cannot be removed at all.
Every open file takes 12 bytes of heap memory.
Used file-space is counted separately from the heap and stack. If you use more memory for files your program will be killed.
You program will be called several times. Make sure to close and remove all auxiliary files and to free memory, as not doing so may result in error in a subsequent run.
Reading and writing data one by one has a big time overhead, use buffers in the API appropriately.




***Recurtion Task Discription***
After a few months that you spend waiting at the end of the calculations necessary for your research, you lost your patience. It is not possible, no one can do good research in these conditions! You decided to quit your job at the university and find a better place to make a big money. You hope that someone will employ a burned-out mathematician.

In the last few years, there is a political project of one of the richest people in the known universe. His wealth comes from large farms producing blurggs and from the production of forbidden chemical fertilizers. At the beginning of his political career, he convinced many creatures by giving out the roasted porgs, however, in a time of crisis caused by the virus, his position is threatened.

The government is pushed to introduce pretty drastic precautions to stop the spread of the virus. On the other hand, there will be elections in the next few weeks, so they must take care not to lose their popularity.

This is a situation as if made for you. There are not many people as educated and willing as you are, who can prepare this devil’s plan to write an algorithm that finds the best sequence of precaution changes such that the political party wins all the elections and minimizes the number of infected people.

After the presentation of your abilities to the Supreme Chancellor, you got a new job.

At first, you analyze the whole situation. You have a list of possible precautions. For every precaution, there is an impact on the reproduction number of the virus and an impact on the popularity of your political party. Moreover, politicians in the Senate are too lazy to vote more than once a week. Based on that you can either introduce or cancel a single precaution every Monday. Every precaution can be introduced and canceled at most once and a first precaution can be introduced in the second week.

According to the research of our PR experts, we can say that if the number of infected exceeds 2^30, then there will be rebellion in the universe and we will be executed. The same situation occurs when our popularity is smaller than 0. On the other hand, the maximum value of our popularity cannot exceed 100, i.e. if the popularity would get bigger than 100, then it stops at 100.

The growth of the number of infected creatures is counted at the end of every week. We can obtain this number as: floor(0.00000001 + # of infected in previous week</ var> * (R * dR of every active precaution)).

For every election, we know the minimum required popularity that is needed to win. If we are not able to win all the elections, then we will be replaced by the opposition. Every week there will be no more than single elections, and elections are always held on Wednesdays.

Your goal is to write an algorithm that finds an arbitrary sequence of introductions/cancelations of precautions such that all the above conditions are satisfied, our political party wins all the elections, and the total number of infected creatures at the end is minimized. Moreover, if such a sequence cannot be found, you must detect it.

**Input format**
Input consists of multiple test data. Therefore, on the first line, there is a number T representing the number of following test cases that need to be correctly processed.

Every test case starts with septet of numbers: N, A, O, P, F, V, and R; where N stands for number of weeks, A is maximum number of concurrently active precautions, O is a number of available precautions that will be specified later on the input, P is number of elections that will be specified later on the input, F is our initial popularity, V is the number of infected people at the end of the first week, R is a real number representing global spreading ability of the virus. All the numbers except R are integers.

Next O lines describe available precautions. Every line consists of four numbers, the first three numbers represent the change in popularity when the precaution is introduced / active / canceled, and the fourth number dR is the impact on the virus spreading ability. All the numbers excluding dR are integers.

Next P lines describe elections. Each line contains two integers. The first number is the week when the elections take place, and the second is minimal popularity required to win the elections.

You can rely on following bounds:
T ≤ 100
N ≤ 100
A ≤ 5
O ≤ 6
0 ≤ P ≤ N-1
0 ≤ F ≤ 100
0 ≤ V ≤ 200000
0 < R < 4
0 < dR < 2

**Output format**
On the first line print two numbers Z and X, where Z is the number of precautions changes, and X is the number of infected creatures at the end of the monitored period.

Next Z lines will contain information about precautions changes. Every line will describe a single change and consist of three numbers – a week of precaution change, precaution change type where zavest stands for precaution introduction and zrusit stands for cancelation, and the ID of the precaution that is changed (integer from range 0 to O - 1).

If the sequence does not exists, display -1.